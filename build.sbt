name := "parser"
 
scalaVersion := "2.9.1"


resolvers += "Java.net Maven2 Repository" at "http://download.java.net/maven/2/"


libraryDependencies ++= {
  val liftVersion = "2.4"
  Seq(
    "org.ccil.cowan.tagsoup" % "tagsoup" % "1.2"
  )    
}