package tools.ly

import java.io.{PrintWriter, File}
import io.Source
import xml.{Node, XML, Unparsed}
import java.util.regex.Pattern
import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 2/15/13
 * Time: 7:45 PM
 * To change this template use File | Settings | File Templates.
 */
object Parser {
  var details = List[(String, String)]()
  def main(args: Array[String]) {

    val folderPath = args(0)
    val file = new File(folderPath)
    file.isDirectory match {
      case true =>
        file.listFiles().map(parseFolder)
      case _ => println("Path given is not a directory")
    }
    details.foreach(println)
  }

  def parseFolder(file: File) {
    file.isDirectory match {
      case true =>
        println("Processing folder: "+file.getName)
        val outFile = new File("/home/shwetanka/csv/"+file.getName+".csv")
        if (!outFile.exists()){
          outFile.createNewFile()
        }
        val out = new PrintWriter(outFile)
        out.write("\"Company\", \"Company Type\", \"Industry\", \"Sector\", \"Amount\", \"Round\", \"Stage\", \"Investors\", \"Investors Type\", \"Stake(%)\"" +
          "\"Date\", \"City\", \"Region\", \"Website\", \"Advisor Company\", \"Advisor Investors\", \"More Details\", \"Link\"\n")
        file.listFiles().map(parseFile).map(ls => out.write((ls.map("\""+_._1+"\"").mkString(","))+"\n"))
        out.flush()
        out.close()
        println("Processing done for: "+file.getName)
      case _ => println("Ignoring non directory - "+file.getName)
    }
  }

  def parseFile(file: File): List[(String, String)] = {
    println("Processing file: "+file.getName)
    var data = List[(String, String)]()
    val xml = XML.withSAXParser(new SAXFactoryImpl().newSAXParser()).loadFile(file)
    val trs = ((xml \ "body" \ "form" \ "table")(1) \ "tr")
    trs.slice(1, trs.size-1).map(tr => {
      var lst = List[String]()
      (tr \ "td").foreach(
        td => td.find(p => p.label.equals("table")) match {
            case Some(table) =>
              lst = (table \ "tr" \ "td").map(_.text.trim.replace("\n", "").replace("\t", "").replace("\r", "")).mkString(";") :: lst
            case _ =>
              lst = td.text.trim :: lst
        }
      )
      data = data :+ (lst(0), lst(1))
    })
    data
  }
}
