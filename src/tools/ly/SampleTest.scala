package tools.ly

import java.io.{PrintWriter, File}
import io.Source
import xml.{Node, XML, Unparsed}
import java.util.regex.Pattern
import org.ccil.cowan.tagsoup.jaxp.SAXFactoryImpl

/**
 * Created with IntelliJ IDEA.
 * User: shwetanka
 * Date: 2/15/13
 * Time: 7:45 PM
 * To change this template use File | Settings | File Templates.
 */
object SampleTest {
  var details = List[(String, String)]()
  def main(args: Array[String]) {

    val file = new File("/home/shwetanka/html/test")
    file.isDirectory match {
      case true =>
        file.listFiles().map(parseFile)
      case _ => println("Path given is not a directory")
    }
    details.foreach(println)
  }

  def parseFile(file: File) = {
    println("Processing file: "+file.getName)
    val xml = XML.withSAXParser(new SAXFactoryImpl().newSAXParser()).loadFile(file)
    val trs = ((xml \ "body" \ "form" \ "table")(1) \ "tr")
    trs.slice(1, trs.size-1).map(tr => {
      (tr \ "td").foreach(
        td => td.find(p => p.label.equals("table")) match {
          case Some(table) =>
            val tem = (table \ "tr" \ "td").map(_.text).mkString(";")
            print(tem)
          case _ =>
            td.text.toCharArray.foreach(_.asDigit)
        }
      )
      println()
    })
  }
}
